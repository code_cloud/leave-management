import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  private backendApi = environment.api
  private urlPath = ''

  private urls = {
    login: '/authenticate/login',
    viewLeave: '/leave-management/view-leave',
    createLeave: '/leave-management/create-leave'
  }

  constructor(private http: HttpClient) {
  }

  // Build headers
  private getHeaders() {

    let headers = new HttpHeaders()

    headers = headers.append('x-source', 'portal')

    if (sessionStorage.getItem("XUSERID")) {
      headers = headers.append('x-user', sessionStorage.getItem("XUSERID"))
    }

    if (sessionStorage.getItem("XTOKEN")) {
      headers = headers.append('x-token', sessionStorage.getItem("XTOKEN"))
    }

    return headers
  }

  // Function to get the full URL needed for call
  private getFullURL(url) {
    return this.backendApi + this.urls[url];
  }

  // Function to do GET requests
  get(key, params) {

    this.urlPath = this.getFullURL(key)

    if (params) {
      this.urlPath += params
    }

    let headers = this.getHeaders()
    let options = {
      headers: headers
    }

    return this.http.get<any>(this.urlPath, options)
  }

  // Function to do POST requests
  post(key, body) {

    this.urlPath = this.getFullURL(key)

    let headers = this.getHeaders()
    let options = {
      headers: headers
    }

    return this.http.post<any>(this.urlPath, body, options)
  }

  // Function to do PUT requests
  put(key, params, body) {

    this.urlPath = this.getFullURL(key)

    if (params) {
      this.urlPath += params
    }

    let headers = this.getHeaders()
    let options = {
      headers: headers
    }

    return this.http.put<any>(this.urlPath, body, options)
  }

  // Function to do DELETE request
  delete(key, params) {

    this.urlPath = this.getFullURL(key)

    if (params) {
      this.urlPath += params
    }

    let headers = this.getHeaders()
    let options = {
      headers: headers
    }

    return this.http.delete<any>(this.urlPath, options)
  }
}