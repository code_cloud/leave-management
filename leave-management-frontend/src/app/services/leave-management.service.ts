import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ApiService } from './api.service';

@Injectable()
export class LeaveManagementService {

  private viewLeaveKey = 'viewLeave'
  private createLeaveKey = 'createLeave'

  constructor(private apiService: ApiService) { }

  viewLeave(data) {
    return this.apiService.post(this.viewLeaveKey, data)
  }

  createLeave(data) {
    return this.apiService.post(this.createLeaveKey, data)
  }

}