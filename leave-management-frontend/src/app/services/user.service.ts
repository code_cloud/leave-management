import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ApiService } from './api.service';

@Injectable()
export class UserService {

  private userKey = 'users'

  private user = new BehaviorSubject<any>({});
  userData = this.user.asObservable();

  constructor(private router: Router,
    private apiService: ApiService) { }

  updateUser(val) {
    this.user.next(val)
  }

}