import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeaveManagementComponent } from './leave-management.component';
import { CreateLeaveComponent } from './create-leave/create-leave.component';
import { ViewLeaveComponent } from './view-leave/view-leave.component';

const routes: Routes = [
  {
    path: '', component: LeaveManagementComponent, children: [
      { path: 'create-leave', component: CreateLeaveComponent },
      { path: 'view-leave', component: ViewLeaveComponent }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaveManagementRoutingModule { }
