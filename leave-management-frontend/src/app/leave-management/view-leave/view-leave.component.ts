import { Component, OnInit } from '@angular/core';
import { LeaveManagementService } from 'src/app/services/leave-management.service';

@Component({
  selector: 'app-view-leave',
  templateUrl: './view-leave.component.html',
  styleUrls: ['./view-leave.component.scss']
})
export class ViewLeaveComponent implements OnInit {

  public leaveData: any
  public isSortedClicked: boolean
  public isAsc: boolean
  public selectedColumn: string

  constructor(private leaveService: LeaveManagementService) {
    this.isSortedClicked = false;
    this.isAsc = false;
    this.selectedColumn = '';
  }

  ngOnInit(): void {
    this.leaveService.viewLeave({})
      .subscribe(res => {
        this.leaveData = res.response.leaveData
      })
  }

  onSortUsers(column: string) {

    this.isSortedClicked = true

    if (column !== this.selectedColumn) {
      this.isAsc = false
    }

    this.selectedColumn = column
    this.isAsc = !this.isAsc

    switch (column) {
      case 'name':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.name > b.name) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.name < b.name) ? 1 : -1)
        }
        break;
      case 'surname':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.surname > b.surname) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.surname < b.surname) ? 1 : -1)
        }
        break;
      case 'type':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.type > b.type) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.type < b.type) ? 1 : -1)
        }
        break;
      case 'status':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.status > b.status) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.status < b.status) ? 1 : -1)
        }
        break;
      case 'days':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.days > b.days) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.days < b.days) ? 1 : -1)
        }
        break;
      case 'from':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.from > b.from) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.from < b.from) ? 1 : -1)
        }
        break;
      case 'to':
        if (this.isAsc) {
          this.leaveData.sort((a, b) => (a.to > b.to) ? 1 : -1)
        } else {
          this.leaveData.sort((a, b) => (a.to < b.to) ? 1 : -1)
        }
        break;
      default:
        break;
    }

  }

}
