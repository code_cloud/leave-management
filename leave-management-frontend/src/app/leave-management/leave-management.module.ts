import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeaveManagementRoutingModule } from './leave-management-routing.module';
import { LeaveManagementComponent } from './leave-management.component';
import { CreateLeaveComponent } from './create-leave/create-leave.component';
import { ViewLeaveComponent } from './view-leave/view-leave.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  declarations: [
    LeaveManagementComponent, 
    CreateLeaveComponent, 
    ViewLeaveComponent, 
    MenuComponent
  ],
  imports: [
    CommonModule,
    LeaveManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
  ]
})
export class LeaveManagementModule { }
