import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LoginService } from '../../services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  @ViewChild('logoutModal') logoutModal: ElementRef
  public modalRef: BsModalRef
  public menu: any
  private userData: any

  constructor(
    private loginService: LoginService,
    private modalService: BsModalService,
    private router: Router,
    private userService: UserService) {

      this.menu = [
        {
          caption: 'Create Leave Request',
          route: 'create-leave',
          roles: ['employee', 'employer']
        },
        {
          caption: 'View Leave Requests',
          route: 'view-leave',
          roles: ['employer']
        }
      ]
    
      this.userService.userData.subscribe(user => {
        this.userData = user

        this.menu.forEach((entry, index) => {
          if (!entry.roles.includes(this.userData.type)) {
            this.menu.splice(index, 1)
          }
        });
      })

  }

  ngOnInit() {

  }

  showLogoutModal() {
    this.modalRef = this.modalService.show(this.logoutModal)
  }

  hideModal() {
    this.modalRef.hide()
  }

  logout() {
    this.hideModal()
    this.loginService.logout()
  }

}
