import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '../../services/user.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LeaveManagementService } from '../../services/leave-management.service';

@Component({
  selector: 'app-create-leave',
  templateUrl: './create-leave.component.html',
  styleUrls: ['./create-leave.component.scss']
})
export class CreateLeaveComponent implements OnInit {

  @ViewChild('addModal') addModal: ElementRef
  public leaveForm: FormGroup
  public modalRef: BsModalRef
  public userData: any
  public isLoading: boolean
  public leaveOptions: any[]
  public selectedLeave: string
  public isSubmitted: boolean
  public errorMgs: string
  public selectedStartDate: any
  public selectedEndDate: any
  public startDateIsSelected: boolean
  public error: boolean
  public today: Date
  public totalDays: number

  constructor(private userService: UserService,
    formBuilder: FormBuilder,
    private modalService: BsModalService,
    private leaveService: LeaveManagementService) {
    this.isLoading = true

    this.leaveForm = formBuilder.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      type: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
    })

    this.leaveOptions = [
      {
        type: 'annual',
        caption: 'Annual'
      },
      {
        type: 'sick',
        caption: 'Sick'
      },
      {
        type: 'family',
        caption: 'Family'
      },
      {
        type: 'study',
        caption: 'Study'
      }
    ]

    this.isSubmitted = false
    this.startDateIsSelected = false
    this.errorMgs = ''
    this.error = false
    this.today = new Date()
    this.totalDays = 0

    this.userService.userData
      .subscribe(user => {
        this.userData = user
        this.leaveForm.controls['name'].setValue(this.userData.name)
        this.leaveForm.controls['surname'].setValue(this.userData.surname)
      })
      this.isLoading = false
  }

  ngOnInit(): void {
    this.isLoading = true
    this.isLoading = false
  }

  get formControls() {
    return this.leaveForm.controls
  }

  createRequest() {

    this.isSubmitted = true;
    this.error = false;

    if (this.leaveForm.invalid) {
      return
    }

    this.errorMgs = ''

    this.totalDays = this.calcBusinessDays(new Date(this.leaveForm.get("startDate").value), new Date(this.leaveForm.get("endDate").value))
    let leaveOption = this.leaveOptions.find(item => item.caption === this.leaveForm.get("type").value).type
    let availableDays = this.userData.leaveRemaining[leaveOption]

    if (this.totalDays > availableDays) {
      this.errorMgs = 'Requested leave days exceeds available days!'
      this.error = true
      this.showAddModal()
      return
    }

    this.userData.leaveRemaining[leaveOption] -= this.totalDays

    const payload = {
      leaveData: {
        'name': this.leaveForm.get("name").value,
        'surname': this.leaveForm.get("surname").value,
        'userId': this.userData._id,
        'type': leaveOption,
        'days': this.totalDays,
        'from': new Date(this.leaveForm.get("startDate").value),
        'to': new Date(this.leaveForm.get("endDate").value)
      },
      leaveRemaining: {
        ...this.userData.leaveRemaining
      }
    }

    this.leaveService.createLeave(payload)
    .subscribe(res => {
      if (res.success) {
        this.error = false
        this.userService.updateUser(this.userData)
        this.showAddModal()
      }
    },
      error => {
        this.errorMgs = 'Unable to authenticate!'
        this.error = true
        this.showAddModal()
      });

  }

  calcBusinessDays(dDate1, dDate2) {
 
    let iWeeks, iDateDiff, iAdjust = 0;
   
    if (dDate2 < dDate1) return -1;
   
    let iWeekday1 = dDate1.getDay();
    let iWeekday2 = dDate2.getDay();
   
    iWeekday1 = (iWeekday1 == 0) ? 7 : iWeekday1;
    iWeekday2 = (iWeekday2 == 0) ? 7 : iWeekday2;
   
    if ((iWeekday1 > 5) && (iWeekday2 > 5)) iAdjust = 1;
    iWeekday1 = (iWeekday1 > 5) ? 5 : iWeekday1;
    iWeekday2 = (iWeekday2 > 5) ? 5 : iWeekday2;
   
    iWeeks = Math.floor((dDate2.getTime() - dDate1.getTime()) / 604800000)
   
    if (iWeekday1 <= iWeekday2) {
      iDateDiff = (iWeeks * 5) + (iWeekday2 - iWeekday1)
    } else {
      iDateDiff = ((iWeeks + 1) * 5) - (iWeekday1 - iWeekday2)
    }
   
    iDateDiff -= iAdjust
   
    return (iDateDiff + 1)
   
  }

  startDateSelected(date) {
    
    if (date) {
      this.startDateIsSelected = true
    }
  }

  endDateSelected(date) {
    if (date) {
      this.totalDays = this.calcBusinessDays(new Date(this.leaveForm.get("startDate").value), new Date(this.leaveForm.get("endDate").value))
    }
  }

  showAddModal() {
    this.modalRef = this.modalService.show(this.addModal)
  }

  hideModal() {
    this.modalRef.hide()
  }

}
