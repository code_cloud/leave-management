# Project is divided into two repo's (frontend and backend)

# Run docker-compose
```
docker-compose up
```

# Run frontend
```
cd leave-management-frontend
npm i
ng serve
```

# Run backend
```
cd leave-management-backend
npm i
npm run start
```