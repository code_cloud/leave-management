import { Injectable } from '@nestjs/common';
import { CalendarService } from '../services/calendar/calendar.service';
import { MongoService } from '../services/mongo/mongo.service';

@Injectable()
export class LeaveManagementService {

    private readonly usersCollection: string = 'users'
    private readonly leaveCollection: string = 'leave'

    constructor(private mongoService: MongoService, private calendarService: CalendarService) { }

    async createLeave(body: any) {

        const leaveData = body.leaveData
        leaveData.status = 'pending'
        leaveData.to = new Date(leaveData.to)
        leaveData.from = new Date(leaveData.from)
        const userId = body.leaveData.userId
        const leaveRemaining = body.leaveRemaining

        let updateUserResult = await this.mongoService.updateOneMongo(this.usersCollection, userId, { leaveRemaining: leaveRemaining })
        let updateLeaveResult = await this.mongoService.insertOneMongo(this.leaveCollection, leaveData)

        // The google calendar event to add
        let calendarEvent = {
            'summary': 'Leave: ' + leaveData.name + ' ' + leaveData.surname,
            'description': 'Leave Type: ' + leaveData.type + ', Total Days: ' + leaveData.days,
            'start': {
                'dateTime': leaveData.from,
                'timeZone': 'CAT',
            },
            'end': {
                'dateTime': leaveData.to,
                'timeZone': 'CAT',
            },
        }
        this.calendarService.addEvent(calendarEvent)

        if (updateUserResult && updateLeaveResult) {

            return {
                success: true,
                message: 'Successfully created leave request!',
                response: {
                }
            }
        } else {
            return {
                success: false,
                message: 'Unable to create leave request!'
            }
        }
    }

    async viewLeave(body: any) {

        let query = {
            ...body
        }

        const leaveData = await this.mongoService.findMongo(this.leaveCollection, query)

        if (leaveData) {

            return {
                success: true,
                message: 'Successfully retrieved leave data!',
                response: {
                    leaveData: [
                        ...leaveData
                    ]
                }
            }
        } else {
            return {
                success: false,
                message: 'Unable to find leave data!'
            }
        }
    }

}
