import { Controller, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { LeaveManagementService } from './leave-management.service'

@Controller('leave-management')
export class LeaveManagementController {

    constructor(private leaveManagementService : LeaveManagementService ) {}

    @Post('create-leave')
    async createLeave(@Req() request: Request) {
        const body = request.body
        return this.leaveManagementService.createLeave(body)
    }

    @Post('view-leave')
    async viewLeave(@Req() request: Request) {
        const body = request.body
        return this.leaveManagementService.viewLeave(body)
    } 
}
