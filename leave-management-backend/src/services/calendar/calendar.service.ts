import { Injectable } from '@nestjs/common';
import * as readline from 'readline';
import * as fs from "fs";
import * as util from "util";
import { OAuth2Client } from "google-auth-library";
import { calendar_v3, google } from "googleapis";
const calendar = google.calendar('v3');

const readFile = util.promisify(fs.readFile);

export class Event {
    id?: string | undefined;
    title?: string | undefined;
    date?: string | undefined;
    timeZone?: string | undefined;
    description: string | undefined;
    host: string | undefined;
    maxParticipants?: number | undefined;
    location: string | undefined;
}

@Injectable()
export class CalendarService {

    private readonly SCOPES = ['https://www.googleapis.com/auth/calendar'];
    private readonly TOKEN_PATH = 'token.json';
    private readonly CRED_PATH = 'credentials.json'
    private readonly calendarId = 'primary'

    private calendar: calendar_v3.Calendar;
    private auth: any

    constructor() {

        this.authorizeClient().then((auth: any) => {
            this.calendar = google.calendar({ version: "v3", auth: auth })
            this.auth = auth
        })

    }

    private authorizeClient(): Promise<any> {
        return readFile(this.CRED_PATH).then((credentials) => {
            const { client_secret, client_id, redirect_uris } =
                JSON.parse(credentials.toString()).installed;
            const oAuth2Client = new google.auth.OAuth2(
                client_id, client_secret, redirect_uris[0]);
            return readFile(this.TOKEN_PATH).then((token) => {
                oAuth2Client.setCredentials(JSON.parse(token.toString()));
                return oAuth2Client;
            });
        }).catch((err) => {
            console.log('Authentication Error:', err);
            throw err;
        });
    }

    private calendarList(auth, calendar) {
        calendar.events.list({
            calendarId: this.calendarId,
            timeMin: (new Date()).toISOString(),
            maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime',
        }, (err, res) => {
            if (err) {
                return console.log('Error:', err);
            }
            const events = res.data.items;
            if (events.length) {
                console.log('Upcoming 10 events:');
                events.map((event, i) => {
                    const start = event.start.dateTime || event.start.date;
                    console.log(`${start} - ${event.summary}`);
                });
            } else {
                console.log('No upcoming events found.');
            }
        });
    }

    private calendarAdd(auth, calendar, calendarEvent) {

        calendar.events.insert({
            auth: auth,
            calendarId: this.calendarId,
            resource: calendarEvent,
        }, (err, res) => {
            if (err) {
                console.log('Error:', err);
                return
            }
        });
    }

    public getEvent() {
        this.calendarList(this.auth, this.calendar)
    }

    public addEvent(calendarEvent) {
        this.calendarAdd(this.auth, this.calendar, calendarEvent)
    }
}
