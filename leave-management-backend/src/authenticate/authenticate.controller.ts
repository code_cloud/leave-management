import { Controller, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { AuthenticateService } from './authenticate.service';

@Controller('authenticate')
export class AuthenticateController {

    constructor(private authService : AuthenticateService ) {}

    @Post('login')
    async authenticate(@Req() request: Request) {
        const body = request.body
        return this.authService.authenticate(body)
    } 
}