import { Injectable } from '@nestjs/common';
import { MongoService } from '../services/mongo/mongo.service';
import { TokenService } from '../services/token/token.service';

@Injectable()
export class AuthenticateService {

    private readonly usersCollection: string = 'users'

    constructor(
        private mongoService: MongoService,
        private tokenService: TokenService
    ) { }

    async authenticate(body: any) {

        const query = { "username": body.username }

        const user = await this.mongoService.findMongo(this.usersCollection, query)

        if (user[0]?.password === body.password) {

            delete user[0].password
            const token = await this.tokenService.generateToken(user[0]._id)
            user[0].token = token

            const response = {
                ...user[0]
            }

            return {
                success: true,
                message: 'Successfully logged in!',
                response: {
                    ...response
                }
            }
        } else {
            return {
                success: false,
                message: 'Incorrect user details'
            }
        }
    }
}
