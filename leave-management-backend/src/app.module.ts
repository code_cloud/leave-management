import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { MongoModule } from 'nest-mongodb';
import { MongoService } from './services/mongo/mongo.service';
import { AuthenticateMiddleware } from './middleware/authenticate.middleware';
import { TokenService } from './services/token/token.service';
import { AuthenticateController } from './authenticate/authenticate.controller';
import { AuthenticateService } from './authenticate/authenticate.service';
import { LeaveManagementController } from './leave-management/leave-management.controller';
import { LeaveManagementService } from './leave-management/leave-management.service';
import { CalendarService } from './services/calendar/calendar.service';

@Module({
  imports: [
    MongoModule.forRoot('mongodb://mongo', 'leave-management', {
      useUnifiedTopology: true
    })
  ],
  controllers: [
    AuthenticateController,
    LeaveManagementController
  ],
  providers: [
    MongoService,
    TokenService,
    AuthenticateService,
    LeaveManagementService,
    CalendarService
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthenticateMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL })
  }
}
